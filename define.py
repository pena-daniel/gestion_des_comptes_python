
import random
import string

accounList = []
codeLenght = 3

def initAccount(): 
    return  {
    "code": "",
    "nom": "",
    "taux": 100,
    "percentRest": 100,
    "percentFrom": 0,
    "solde": 0,
    "salBase": 0,
    "parentCode":"", 
    "undder": []
    }

def defineAccountProps(account, parent = 0,isSub = False):
    if isSub  and int(parent["percentRest"]) == 0:
        print('le compte est vide veuillez renflouer pour creer un sous compte')
    else:
        nom= input("quel est le nom du compte ? ")
        res1 = checkName(nom)
        while res1 != 0:
            nom = input("rentrer a nouveau le nom car celui entrer est deja utilisé : ")
            res1 = checkName(nom)
        print('votre nom est : '+ nom)
        account["nom"] = nom
        account["code"] = generateCode(codeLenght) 
        print('votre code est : ' + account["code"])
        if isSub:
            percent = input("quel pourcentage affecter vous?")
            res = setPercent(parent, account, percent) 
            while res != 1:
                percent = input("choisissez a nouveau le pourcentage?")
                res = setPercent(parent, account, percent)
            print('le pourcentage est : '+ percent)
            account['parentCode'] = parent['code']
            soldCalc = (int(parent["salBase"]) * int(account['percentFrom']))/100
            setAmount(account,soldCalc, True)
            # setAmount(parent,(int(parent["solde"])  - soldCalc), False)
            print('le solde est : {}'.format(soldCalc))
            parent['undder'].append(account['code'])
        else :
            amt = input("avec combien voulez vous commencer?")
            res1 = setAmount(account, amt, True)
            while res1 != 1:
                amt = input("rentrer a nouveau la somme?")
                res1 = setAmount(account, amt, True)
            print('le solde est : ' + amt)
        accounList.append(account)

def setAmount(account, amount, str = False):
    try:
        account["solde"] = int(amount)
        if str:
            account["salBase"] = account["solde"]
        return 1
    except:
        print("il n'y a que les nombres qui sont alloués")

def checkName(name):
    for item in accounList:
        if item['nom'] == name:
            return 1
    return 0
        
def setPercent(parent, account, taux):
    try:
        percent = int(taux)
        if (parent["percentRest"] >= percent) & (percent > 0):
            parent["percentRest"] = int(parent["percentRest"]) - percent
            account["percentFrom"] = percent
            return 1
        else: 
            print("vous n'avez plus que {}'%' sur ce compte".format(parent['percentRest']))
            return 0
    except :
        print("uniquement les nombres entre 0 et 100")
        return 0
        
def generateCode(lenght):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(lenght))
     
def getTotalAmount():
    for item in accounList:
        if item['parentCode'] == '':
            print('solde total : {}'.format(item['solde']))
            return 0
        

def transasction(account, amount, opt = "rmv", fromChild = False, acc = 1):
    if account['parentCode'] == '':
        acc = 1
        if opt == 'rmv' and fromChild == False:
            print('impossible de retirer de l\'argent dans ce compte')
            return 0
        else:
            if fromChild == False:   
                amountRest = (int(account['salBase'])*(account['percentRest']))/100
                account['percentRest'] = 100
                account['salBase'] = amountRest + amount
                account['solde'] = int(account['solde']) + amount
            else:
                if opt == 'rmv':
                    account['solde'] = int(account['solde']) - amount
                else:
                    account['solde'] = int(account['solde']) + amount
        return 1
    state = 'end'
    amountRest = (int(account['salBase'])*(account['percentRest']))/100
    accRest = amountRest - amount
    if opt == "rmv":
        if acc == 0 and accRest >= 0 :
            account['percentRest'] = 100
            account['salBase'] = accRest
        elif acc == 0 and accRest < 0:
            print('imposssible d\'effectuer se retrait : solde insuffisant')
            return 0
        rest = int(account['solde']) - amount
        if int(rest) >= 0:
            account['solde'] = rest
        else:
            print('imposssible d\'effectuer se retrait : solde insuffisant')
            return 0
    else:
        if acc == 0:
            account['percentRest'] = 100
            account['salBase'] = amountRest + amount
        account['solde'] = int(account['solde'] ) + amount
    for item in accounList:
        if item['code'] == account['parentCode']:
            account = item
            state = 'continue'
            break
    if state == 'continue':    
        transasction(account, amount, opt, True)
    else:
        return 1

def checkAccount(code):
    for item in accounList:
        if item['code'] == code:
            return 1
    return 0

def getAccount(code):
    print(code)
    for item in accounList:
        if item['code'] == code:
            return item
    return 0

def getAccountCode(message = 'à quel compte voulez vous le rattachez'):
    print("{}?".format(message))
    print('Liste des comptes')
    print('Code ---------- nom ---------- taux ---------- solde')
    for item in accounList:
        solde = (int(item['salBase']) * int(item['percentRest']))/100
        print('{} ---------- {} ---------- {} ---------- {}'.format(item['code'],item['nom'],item['percentRest'], solde))
    code = input("choisissez le code du compte : ")
    res = checkAccount(code)
    while res != 1:
        code = input("choisissez un compte qui existe : ")
        res = checkAccount(code)
    return code

def createAccount(isSub = False):
    if isSub:
        code = getAccountCode()
        parent = getAccount(code)
        defineAccountProps(initAccount(),parent, True)
    else:
        defineAccountProps(initAccount())
        
def generateMenu():
    print("")
    print("-----------------------------MENU-----------------------------")
    print("")
    print("Que voulez vous faire ?")
    print("1.Creer un compte")
    print("2.Creer un sous-compte")
    print("3.Effectuer une transaction")
    print("4.Solde total")
    print("5.Sortir")
    print("")

def subMenu(num):
    print("")
    if num == 3:
        print("Choisissez l'operation")
        print("1.Ajouter de l'argent")
        print("2.Retirer de l'argent")
    print("")

def getAction():
    generateMenu()
    request = input()
    choice = checkParam(request,3)
    return int(choice)

def checkParam(param, type):
    if type == 1:
        try:
            while int(param) <= 0:
                print('entrer une valeur positive')
                param = input("montant : ")
                return checkParam(param,1)
        except :
            print("Les lettres ou les mots ne sont pas autorisés")
            param = input("nouvelle valeur : ")
            return checkParam(param,1)
    elif type == 2:
        try:
            while int(param) not in [1,2]:
                print("Entre un nombre entre 1 et 2 en fontion de votre operation : ")
                param = input()
                return checkParam(param,2)
        except :
            print("Les lettres ou les mots ne sont pas autorisés")
            param = 0
            while int(param) not in [1,2]:
                print("Entre un nombre entre 1 et 2 en fontion de votre operation : ")
                param = input()
                return checkParam(param,2) 
    elif type == 3:
        try:
            while int(param) not in [1,2,3,4,5]:
                print("Entre un nombre entre 1 et 5 en fontion de votre operation : ")
                param = input()
                return checkParam(param,3)  
        except :
            print("Les lettres ou les mots ne sont pas autorisés")
            param = 0
            while int(param) not in [1,2,3,4,5]:
                print("Entre un nombre entre 1 et 5 en fontion de votre operation : ")
                param = input()
                return checkParam(param,3) 
                  
    return param

        
def execute(num):
    if num == 1:
        print("vous avez choisit creer un compte")
        print()
        createAccount()
    elif num == 2 :
        if len(accounList)  == 0:
            print("veuillez d'abord creer un compte avant d'effectuer ce type d'operation. ")
            print()
            return 0
        print("vous avez choisit creer un sous compte")
        print()
        createAccount(True)
    elif num == 3:
        if len(accounList) == 0:
            print("veuillez d'abord creer un compte avant d'effectuer ce type d'operation. ")
            print()
            return 0
        print("vous avez choisit creer de faire une transaction")
        print()
        subMenu(num)
        choice = input()
        choice = checkParam(choice, 2)
        code = getAccountCode('sur quel compte vouslez vous effectuer cette operation ?')
        account = getAccount(code)
        
        # check amount
        print("quel est le montant de la transaction ?")
        amount = input()
        amount = checkParam(amount,1)
        
        
        if int(choice) == 1:
            choice = "add"
        else:
            choice = "rmv"
        res = transasction(account,int(amount),choice, False,0)
        
        if (account['parentCode'] != '') or (account['parentCode'] == '' and choice == 'add'):
            if res == 1:
                print('transaction terminer : nouveau solde : {}'.format(account['salBase']))
            
    elif num == 4:
        if len(accounList)  == 0:
            print("veuillez d'abord creer un compte avant d'effectuer ce type d'operation.")
            print()
        else:
            getTotalAmount()
            print()
    elif num == 5:
        print("vous avez choisit de quitter")
        print()
        return 0
        
        